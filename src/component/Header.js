import React from 'react'
import "../style/header.css"
export default function Header() {
  return (
    <div id='header' className='container'>
        <h1 className='site-title'>
            <a href="index.html">
                <img src="https://code-step.com/demo/html/profile/img/logo.svg" alt="Profile"/>
            </a>
        </h1>
        <nav>
            <ul>
                <li><a href='#about'>About</a></li>
                <li><a href='#bicycle'>Bicycle</a></li>
            </ul>   
        </nav>
    </div>  
  )
}
