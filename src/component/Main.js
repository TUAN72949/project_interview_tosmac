import React from 'react'
import "../style/main.css"
export default function Main() {
  return (
    <main>
        <div id="mainvisual">
            <img src="https://code-step.com/demo/html/profile/img/mainvisual.jpg" alt="テキストテキストテキスト"/>
        </div>

        <section id="about" className="container">
            <h2 className="section-title">About</h2>
            <div className="content">
                <img src="https://code-step.com/demo/html/profile/img/about.jpg" alt="テキストテキストテキスト"/>
                <div className="text">
                    <h3 className="content-title">KAKERU MIYAICHI</h3>
                    <p>
                        テキストテキストテキストテキストテキストテキストテキスト<br/>
                        テキストテキストテキストテキストテキストテキストテキスト<br/>
                        テキストテキストテキストテキストテキストテキストテキスト
                    </p>
                </div>
            </div>
        </section>

        <section id="bicycle" class="container">
            <h2 class="section-title">Bicycle</h2>
            <ul>
                <li>
                <img src="https://code-step.com/demo/html/profile/img/bicycle1.jpg" alt="テキストテキストテキスト"/>
                <h3 class="content-title">タイトルタイトル</h3>
                <p>テキストテキストテキスト</p>
                </li>
                <li>
                <img src="https://code-step.com/demo/html/profile/img/bicycle2.jpg" alt="テキストテキストテキスト"/>
                <h3 class="content-title">タイトルタイトル</h3>
                <p>テキストテキストテキスト</p>
                </li>
                <li>
                <img src="https://code-step.com/demo/html/profile/img/bicycle3.jpg" alt="テキストテキストテキスト"/>
                <h3 class="content-title">タイトルタイトル</h3>
                <p>テキストテキストテキスト</p>
                </li>
            </ul>
        </section>
  </main>
  )
}
